import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './assets/tailwind.css'

import ButtonPrimary from '@/components/ButtonPrimary'


createApp(App)
    .use(store)
    .use(router)
    .component('ButtonPrimary', ButtonPrimary)
    .mount('#app')
