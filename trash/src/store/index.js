import { createStore } from 'vuex'

export default createStore({
  state: {
    apiBaseUrl: 'http://remoteworkclub.com'
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
