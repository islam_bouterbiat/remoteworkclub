import google from "../assets/images/companies/google.png";
import amazon from "../assets/images/companies/amazon.png";
import github from "../assets/images/companies/github.png";
import basecamp from "../assets/images/companies/basecamp.png";
import invision from "../assets/images/companies/invision.png";

export const companies = [
  { name: "Google", imgUrl: google },
  { name: "Amazon", imgUrl: amazon },
  { name: "Github", imgUrl: github },
  { name: "Basecamp", imgUrl: basecamp },
  { name: "Invision", imgUrl: invision },
];
