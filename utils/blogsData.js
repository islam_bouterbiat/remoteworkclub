export const blogs = [
  {
    title: "How Marketing Made ‘Oil-Free’ A Thing",
    excerpt:
      "Experts say “oil-free” skincare is scam that benefits beauty brands more than it benefits your skin.",
    imageUrl: "/assets/images/blog.jpg ",
    Author: "Islam BTR",
    Date: "03/02/2021",
    Tags: ["Art", "Design"],
  },
  {
    title: "How Marketing Made ‘Oil-Free’ A Thing",
    excerpt:
      "Experts say “oil-free” skincare is scam that benefits beauty brands more than it benefits your skin.",
    imageUrl: "/assets/images/blog.jpg ",
    Author: "Jacob DMN",
    Date: "03/02/2021",
    Tags: ["Art", "Design"],
  },
  {
    title: "How Marketing Made ‘Oil-Free’ A Thing",
    excerpt:
      "Experts say “oil-free” skincare is scam that benefits beauty brands more than it benefits your skin.",
    imageUrl: "/assets/images/blog.jpg ",
    Author: "Steeve",
    Date: "03/02/2021",
    Tags: ["Art", "Design"],
  },
];
