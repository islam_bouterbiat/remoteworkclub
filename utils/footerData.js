export const footerData = [
  {
    name: "Browse Remote Jobs",
    content: ["Job Category 1", "Job Category 2", "Job Category 3"],
  },
  {
    name: "For Remote Workers",
    content: [
      "Reviews",
      "Guides",
      "Health",
      "WFH Setup Inspo",
      "Software &Tools",
      "Lifestyle Design",
      "Start Here If You Want To Be A Remote Worker",
    ],
  },
  {
    name: "For Remote Teams & Businesses",
    content: [
      "Guides",
      "Software & Tools",
      "Growing Businesses",
      "Team Culture",
      "Start Here If You Want To Build A Remote Business",
    ],
  },
  {
    name: "Job Seekers",
    content: [
      "Best Remote Companies",
      "How To Get A Remote Job",
      "Remote Job Resources",
    ],
  },
  {
    name: "Employers",
    content: [
      "Post A Job",
      "Remote Job Description Template",
      "Remote Work Data & Trends",
      "Hiring Guide",
      "Why Post Your Job On RWC?",
      "FAQ",
      "Sign In",
      "Create An Account",
    ],
  },
];
