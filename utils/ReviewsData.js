import image from "../assets/images/daypack.jpg";

export const reviewsCategories = [
  "Customer Experience",
  "Email Newsletters",
  "Sales Process",
];

export const reviews = [
  {
    title: "Lite Daypack & Daypack LGG",
    excerpt:
      "Do you know what a cover letter should look like in 2021? See what a winning cover letter should include to land.",
    slug: "lite-daypack",
    imageUrl: { image },
    Author: "Steeve",
    Date: "03/02/2021",
    numReviews: 10,
    Rating: 9.5,
    Tags: ["Popular", "Excellent Price"],
  },
  {
    title: "Lite Daypack & Daypack LG",
    excerpt:
      "Do you know what a cover letter should look like in 2021? See what a winning cover letter should include to land.",
    slug: "lite-daypack-lg",
    imageUrl: { image },
    Author: "Jacob dmn",
    Date: "03/02/2021",
    numReviews: 10,
    Rating: 8.0,
    Tags: ["Popular", "Excellent Price"],
  },
  {
    title: "Lite Daypack & Daypack LGF",
    excerpt:
      "Do you know what a cover letter should look like in 2021? See what a winning cover letter should include to land.",
    slug: "lite-daypack-daypacks-lg",
    imageUrl: { image },
    Author: "Islam BTR",
    Date: "03/02/2021",
    numReviews: 10,
    Rating: 3.5,
    Tags: ["Popular", "Excellent Price"],
  },
];
