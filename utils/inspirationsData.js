export const inspirations = [
  {
    title: "Making Peace With Your Body Is a Mighty Act of Revolution",
    excerpt: "the three tenets of radical self-love",
    imageUrl: "/assets/images/inspiration.jpg ",
    Author: "Islam BTR",
    Date: "03/02/2021",
    Tags: ["Book", "Story"],
  },
  {
    title: "Making Peace With Your Body Is a Mighty Act of Revolution",
    excerpt: "the three tenets of radical self-love",
    imageUrl: "/assets/images/inspiration.jpg ",
    Author: "Jacob DMN",
    Date: "03/02/2021",
    Tags: ["Book", "Story"],
  },
  {
    title: "Making Peace With Your Body Is a Mighty Act of Revolution",
    excerpt: "the three tenets of radical self-love",
    imageUrl: "/assets/images/inspiration.jpg ",
    Author: "Steeve",
    Date: "03/02/2021",
    Tags: ["Book", "Story"],
  },
];
