import image from "../assets/images/stay-healthy.jpg";

export const guides = [
  {
    title: "Convince Steve to sign your contract from the first day",
    Author: "Steve D.Walton",
    excerpt:
      "Do you know what a cover letter should look like in 2021? See what a winning cover letter should include to land.",
    imageUrl: "/assets/images/stay-healthy.jpg ",
    slug: "stay-healthy",
    Tags: ["Job Seekers", "Remote Workers", "All"],
  },
  {
    title: "Stay Healthy with busy schedules",
    Author: "Steve D.Walton",
    excerpt:
      "Do you know what a cover letter should look like in 2021? See what a winning cover letter should include to land.",
    imageUrl: "/assets/images/stay-healthy.jpg ",
    slug: "stay-healthy-1",
    Tags: ["Job Seekers", "Remote Workers", "All"],
  },
  {
    title: "Stay Healthy with busy schedules",
    Author: "Steve D.Walton",
    excerpt:
      "Do you know what a cover letter should look like in 2021? See what a winning cover letter should include to land.",
    imageUrl: "/assets/images/stay-healthy.jpg ",
    slug: "stay-healthy-2",
    Tags: ["Job Seekers", "Remote Workers", "All"],
  },
];
