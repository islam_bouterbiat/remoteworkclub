const path = require('path')

module.exports = {
    configureWebpack: {
        resolve: {
            alias: {
                '@': path.join(__dirname, 'src'),
                '@assets': path.join(__dirname, 'src', 'assets'),
                '@components': path.join(__dirname, 'src', 'components')
            }
        }
    },
    css: {
        loaderOptions: {
            scss: {
                data: `@import "@/assets/scss/vars.scss";`
            }
        }

    },
    chainWebpack: config => {
        config.plugin('html').tap(args => {
            args[0].title = 'Remote Work Club';
            return args;
        })
    }
};