import React from "react";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import CategoriesNavs from "../Components/General Components/CategoriesNavs";
import Slider from "../Components/General Components/Slider";
import Search from "../Components/Search/Search";

import { guides } from "../utils/guidesData";
import image from "../assets/images/guide.jpg";

const guideCategories = [
  "Category 1",
  "Category 2",
  "Category 3",
  "Category 4",
  "Category 5",
  "Category 6",
  "Category 7",
];

const Guides = () => {
  return (
    <div className="pt-20 mx-auto max-w-8xl">
      <Head>
        <title>Guides</title>
        <link rel="icon" href="/favicon.png" />
      </Head>
      {/* First Guide goes here */}
      <div className="pt-10 shadow-lg md:shadow-none ">
        <div className=" md:px-4 md:max-w-screen-xl 2xl:max-w-screen-2xl overflow-hidden  md:mx-auto">
          <h3 className="m-2 mb-4 md:hidden">Guides</h3>
          <div className="md:px-40 md:flex ">
            <Link href="#">
              <a>
                <div className="relative">
                  <Image src={image} alt={guides[0].title} />
                </div>
              </a>
            </Link>
            <div className="px-0 lg:px-6 relative">
              <div className=" h-full">
                <div className="px-2 flex justify-between">
                  <span className=" text-xl text-gray-400 mt-2">How to</span>
                </div>
                <div className="px-2 py-4">
                  <h1 className="font-bold mb-2 md:w-120 text-4xl">
                    {guides[0].title}
                  </h1>
                  <p className="text-gray-400 text-sm mt-6 ">
                    {guides[0].excerpt}
                  </p>

                  {/* button goes here */}
                  <div className="mt-6">
                    <button className="whitespace-nowrap text-white font-bold py-2 px-4 rounded w-full  md:w-auto">
                      View Guide
                    </button>
                  </div>
                  {/* Tags goes here */}
                  <div className="pt-4 pb-1 mb-2 lg:absolute bottom-0">
                    <div className="flex ">
                      {guides[0].Tags.map((t, i) => (
                        <span
                          key={i}
                          className="inline-block rounded-full px-3 py-1 text-xs font-semibold mr-2 bg-gray-100 text-gray-400"
                        >
                          {t}
                        </span>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Categories Navs */}
      <div className="mx-auto max-w-screen-lg 2xl:max-w-screen-xl 3xl:max-w-screen-2xl">
        <div className="mt-8 md:px-12 ">
          <h1 className="text-center md:text-left">Categories</h1>
          <div className="flex flex-wrap my-6 mx-auto justify-center gap-6 mb-20">
            <CategoriesNavs />
            <CategoriesNavs />
            <CategoriesNavs />
            <CategoriesNavs />
            <CategoriesNavs />
            <CategoriesNavs />
          </div>
        </div>
      </div>
      {/* reviews sliders */}
      <div className="md:px-20 mt-6 overflow-visible">
        {guideCategories.map((c, i) => (
          <Slider
            key={i}
            sliderName="guides"
            sliderExploreName={c}
            data={guides}
            row={true}
          />
        ))}
      </div>

      {/* search section  */}
      <div className="max-w-5xl mx-auto md:py-10 px-4 xl:px-0 2xl:max-w-screen-lg">
        <Search />
      </div>
    </div>
  );
};

export default Guides;
