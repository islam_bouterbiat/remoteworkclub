import React from "react";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import CategoriesNavs from "../Components/General Components/CategoriesNavs";
import Slider from "../Components/General Components/Slider";
import Search from "../Components/Search/Search";

import logo_icon from "../assets/images/logo/logo_icon_green.png";
import community_icon from "../assets/images/community_icon.png";
import { reviews } from "../utils/ReviewsData";
import image from "../assets/images/featuredReviewImage.jpg";

const reviewCategories = [
  "Category 1",
  "Category 2",
  "Category 3",
  "Category 4",
  "Category 5",
  "Category 6",
];
const Reviews = () => {
  return (
    <div className="pt-20 mx-auto max-w-8xl">
      <Head>
        <title>Reviews</title>
        <link rel="icon" href="/favicon.png" />
      </Head>
      {/* First Review goes here */}
      <div className="py-10  shadow-lg md:shadow-none ">
        {/* the word 'Reviews, Guides.. mention here */}
        <div className="md:max-w-screen-xl 2xl:max-w-screen-2xl overflow-hidden  mx-auto">
          <h3 className="m-2 mb-4 md:hidden">Reviews</h3>
          <div className="md:px-44 md:flex ">
            <Link href="#">
              <a>
                <div className="relative">
                  <Image src={image} alt={reviews[0].title} />
                  <span className="absolute px-2 p-0.5 font-bold top-2 left-2 md:top-6 md:-left-5 review-card-cat-badge text-white z-50 shadow-md">
                    Gears
                  </span>
                </div>
              </a>
            </Link>
            <div className="p-2 lg:pt-5 lg:px-6 relative grid place-content-center">
              <div className="">
                <div className="px-2 flex justify-between">
                  <span className=" text-lg text-gray-400">
                    {reviews[0].Author}
                  </span>
                </div>
                <div className="px-2 py-4">
                  <h1 className="font-bold mb-2 md:w-120 text-4xl">
                    {reviews[0].title}
                  </h1>
                  <p className="text-gray-400 text-sm mt-3 ">
                    {reviews[0].excerpt}
                  </p>

                  {/* ratings goes here */}
                  <div className="flex items-end mt-3 gap-3">
                    {/* rwc rating */}
                    <div className="flex items-center justify-start">
                      <div className="relative mr-2 flex">
                        <span className="py-1 px-2 text-xs font-bold leading-none text-white rating-review-badge">
                          {reviews[0].Rating}
                        </span>
                        <span className="absolute -top-20px -right-15px w-30px h-30px p- grid place-content-center rounded-full shadow border border-primary z-1 bg-white">
                          <span className="m-0.5 grid place-content-center">
                            <Image
                              src={logo_icon}
                              alt="rating"
                              className="w-full h-full"
                            />
                          </span>
                        </span>
                      </div>
                    </div>
                    {/* community rating */}
                    <div className="flex items-end justify-start">
                      <div className="relative mr-2 flex">
                        <span className="py-1 px-2 text-xs font-bold leading-none text-white rating-review-badge">
                          {reviews[0].Rating}
                        </span>
                        <span className="absolute -top-20px -right-15px w-30px h-30px p- grid place-content-center rounded-full shadow border border-primary z-1 bg-white">
                          <span className="m-1 grid place-content-center">
                            <Image
                              src={community_icon}
                              alt="community rating"
                              className="w-full h-full"
                            />
                          </span>
                        </span>
                      </div>
                    </div>
                    {/* overall rating */}
                    <span className="inline-flex items-center justify-center py-2 px-1 text-xl font-bold leading-none text-white rating-review-badge">
                      {reviews[0].Rating}
                      <span className="text-xs float-bottom mt-1">/10</span>
                    </span>
                  </div>
                  <div className="mt-8">
                    <button className="whitespace-nowrap text-white font-bold py-2 px-4 rounded w-full md:w-auto">
                      View Review
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      {/* Categories Navs */}
      <div className="mx-auto max-w-screen-lg 2xl:max-w-screen-xl 3xl:max-w-screen-2xl">
        <div className="mt-8 md:px-12 ">
          <h1 className="text-center md:text-left">Categories</h1>
          <div className="flex flex-wrap my-6 mx-auto justify-center gap-6 mb-20">
            <CategoriesNavs />
            <CategoriesNavs />
            <CategoriesNavs />
            <CategoriesNavs />
            <CategoriesNavs />
            <CategoriesNavs />
          </div>
        </div>
      </div>
      {/* reviews sliders */}
      <div className="md:px-20 mt-6 overflow-visible">
        {reviewCategories.map((c, i) => (
          <Slider
            key={i}
            sliderName="reviews"
            sliderExploreName={c}
            data={reviews}
            row={true}
          />
        ))}
      </div>

      {/* search section  */}
      <div className="max-w-5xl mx-auto md:py-10 px-4 xl:px-0 2xl:max-w-screen-lg">
        <Search />
      </div>
    </div>
  );
};

export default Reviews;
