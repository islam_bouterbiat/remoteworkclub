import React from "react";
import Image from "next/image";
import Link from "next/link";
import Head from "next/head";
import moment from "moment";
import { useRouter } from "next/router";
import Search from "../../Components/Search/Search";
import Loader from "../../Components/General Components/Loader";
import { FaCircle } from "react-icons/fa";
import { BsSquare } from "react-icons/bs";
import Slider from "../../Components/General Components/Slider";

import { guides } from "../../utils/guidesData";
import profile from "../../assets/images/steve.png";
import RatingStars from "./../../Components/Reviews/RatingStars";
import image1 from "../../assets/images/image1.jpg";
import image2 from "../../assets/images/image2.jpg";
import image3 from "../../assets/images/image3.jpg";
import image4 from "../../assets/images/image4.jpg";
import image5 from "../../assets/images/image5.jpg";
import image6 from "../../assets/images/image6.jpg";
import image7 from "../../assets/images/image7.png";
import image8 from "../../assets/images/image8.jpg";
import image9 from "../../assets/images/image9.png";

const GuideDetailed = () => {
  const router = useRouter();
  if (router.isFallback) {
    return <Loader />;
  }
  //   filtering guidesData according to the slug recieved
  const detailedGuide = guides.filter(
    (guide) => guide.slug === router.query.slug
  );
  const guide = detailedGuide[0];

  const data = [
    { text: "1.  What's an e-learning course?", active: false },
    { text: "2.  How to start an e-learning project?", active: true },
    { text: "3.  Create a course landing page", active: false },
    { text: "4.  Create a storyboard", active: false },
    { text: "5.  Create practice activities", active: false },
    { text: "6.  Create interactive:activities (advanced)", active: false },
    { text: "7.  Summary", active: false },
  ];

  const dummyData = ["", "", ""];

  return (
    <div className="pt-20 mx-auto max-w-8xl">
      <Head>
        <title>Guide</title>
        <link rel="icon" href="/favicon.png" />
      </Head>
      {/* hero section */}
      {guide && (
        <>
          <div className="guide-hero w-full">
            <div className="container mx-auto px-40 mb-8 pt-4">
              {/* Breadcrumb */}
              <nav className="rounded w-full mb-4">
                <ol className="list-reset flex">
                  <li>
                    <a href="#" className="text-gray-300">
                      Guides
                    </a>
                  </li>
                  <li>
                    <span className="mx-2 text-gray-300">{`>`}</span>
                  </li>
                  <li>
                    <a href="#" className="text-gray-300">
                      how to
                    </a>
                  </li>
                  <li>
                    <span className="mx-2 text-gray-300">{`>`}</span>
                  </li>
                  <li className="text-gray-500">Stay healthy</li>
                </ol>
              </nav>
              {/* hero section ... */}
              <div className="grid grid-cols-1 lg:grid-cols-12 gap-12 py-14">
                <div className="lg:col-span-4 col-span-1 my-auto">
                  <div class="flex flex-col text-center justify-center rounded-lg overflow-hidden shadow-lg max-w-xs my-auto bg-white">
                    <div className="bg-black px-5 py-3 flex items-center">
                      <Image
                        src={profile}
                        height={50}
                        width={50}
                        className="rounded"
                      />
                      <div className="text-white flex flex-col text-left ml-3">
                        <span className="text-xs text-gray-400">By</span>
                        <span>{guide.Author}</span>
                        <span className="text-xs text-gray-400">
                          Co-founder of RemoteWorkClub
                        </span>
                      </div>
                    </div>
                    <div className="px-5">
                      <div className="border-b py-3 border-gray-200 flex flex-wrap">
                        {guide.Tags.map((t, i) => (
                          <span
                            key={i}
                            className="whitespace-nowrap mb-1 inline-block rounded-full px-3 py-1 text-xs font-semibold mr-1 bg-gray-100 text-gray-400"
                          >
                            {t}
                          </span>
                        ))}
                      </div>
                    </div>
                    <div className="flex flex-col px-5 py-3">
                      <h1 className="text-lg font-semibold">
                        89% found this article helpful
                      </h1>
                      <div className="flex items-center justify-between">
                        <RatingStars value={9} />
                        <span className="text-xs text-gray-500 ml-4 mt-1">
                          789 vote - 89%
                        </span>
                      </div>
                      <span className="primary-color text-xs text-left mt-2">
                        Click a star to add your vote
                      </span>
                    </div>
                  </div>
                </div>
                <div className="lg:col-span-8 col-span-1">
                  <span className="whitespace-nowrap mb-1 inline-block rounded-full px-3 py-1 text-xs mr-1 bg-emerald-100 text-gray-700">
                    12 min read
                  </span>
                  <h1 className="my-5">
                    An Inspiration to Creating E-Learning docs.
                  </h1>
                  <p className="text-gray-500">
                    An e-learning course can be accessed either via your laptop,
                    phone or tablet, or maybe all of them. It is the era of
                    mobile learning. You can spend minutes or 1 hour to finish
                    learning a new topic, all at your fingertips. <br />
                    <br></br>
                    Have you ever wondered what a good e-learning course looks
                    like? You can find a lot of examples on Udemy, Coursera, and
                    Lynda.com. I have listed some interesting examples below.
                    Check them out before proceeding to see how to get started
                    on your own project.
                  </p>
                </div>
              </div>
            </div>
          </div>
          {/* Body */}
          <div className="container mx-auto px-40 mb-8">
            <div className="grid grid-cols-1 lg:grid-cols-12 gap-12 pb-14 relative">
              <div className="lg:col-span-4 col-span-1 sticky">
                <div class="max-w-xs text-black  bg-gray-50 rounded border border-gray-50 py-3">
                  {data.map((d, i) => (
                    <button
                      key={i}
                      type="button"
                      class="inline-flex justify-between gap-2 items-center text-left relative py-3 px-6 w-full text-sm font-medium hover:bg-gray-200 hover:text-gray-800"
                    >
                      <span className="grow">{d.text}</span>
                      {d.active ? (
                        <FaCircle
                          size={16}
                          className="text-[#14CDA1] flex-none"
                        />
                      ) : (
                        <FaCircle
                          size={16}
                          className="text-gray-300 flex-none"
                        />
                      )}
                    </button>
                  ))}
                </div>
              </div>
              <div className="lg:col-span-8 col-span-1 py-6">
                {/* First Nav content */}
                <div id="#1">
                  <div className="mb-6">
                    <div className="ml-2 mb-6">
                      <h5 className="font-semibold">
                        1. What's an e-learning course?
                      </h5>
                      <span className="block border-b-4 rounded border-[#14CDA1] w-28 mt-3"></span>
                    </div>
                    <Image src={image1} />
                    <div>
                      <h6 className="font-semibold mt-1">Welcome</h6>
                      <p className="text-sm my-3 leading-relaxed text-gray-700">
                        Maybe your first question is, "How should I get
                        started?" It can be scary if it's your first time to
                        create a course or you just simply want to improve your
                        process. Normally, the client who requests you to do a
                        course will have some basic ideas on what they want.
                        Your first task is to dig it up.
                      </p>
                      <h6 className="font-semibold mb-1">
                        Step 1: Understand what they want from you
                      </h6>
                      <p className="text-sm text-gray-700">
                        In the first meeting, ask the following questions to the
                        clients
                      </p>
                      <div className="px-3 py-4 text-gray-700 text-sm">
                        <div className="flex items-center mb-2">
                          <BsSquare className="text-[#14CDA1] mr-4" />
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit.
                        </div>
                        <div className="flex items-center mb-2">
                          <BsSquare className="text-[#14CDA1] mr-4" />
                          Ac, arcu massa curabitur vitae vel
                        </div>
                        <div className="flex items-center mb-2">
                          <BsSquare className="text-[#14CDA1] mr-4" />
                          Egestas scelerisque in gravida. Aliquam et tellus
                          purus fermentum.
                        </div>
                        <div className="flex items-center">
                          <BsSquare className="text-[#14CDA1] mr-4" />
                          Volutpat, sed orci, purus egestas.
                        </div>
                      </div>
                    </div>
                    <div className="relative w-full flex items-center justify-between bg-[#F3F3F3] px-4 py-3 rounded ">
                      <span className="w-2 h-full bg-[#14CDA1] rounded-l absolute left-0"></span>
                      <div className="text-gray-500 text-xs ml-2">
                        <b>Tip:</b> Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit. Id amet libero cursus nec dis quis at
                        egesllis cras gravida nulla maecenas et.
                      </div>
                    </div>
                  </div>
                  <div className="relative mt-4 h-10 flex items-center">
                    <span className="block border-b border-[#14CDA1] w-full "></span>
                    <button className="absolute left-1/2 w-10 h-10 text-white bg-[#14CDA1] transition-colors duration-150 rounded-full focus:shadow-outline ">
                      1
                    </button>
                  </div>
                </div>

                {/* Second Nav content */}
                <div id="#2" className="mt-4">
                  <div className="mb-6">
                    <div className="ml-2 mb-6">
                      <h5 className="font-semibold">
                        2. Classify e-learning into a certain level
                      </h5>
                      <span className="block border-b-2 border-[#14CDA1] w-28 mt-3"></span>
                    </div>
                    <Image src={image2} />
                    <div>
                      <h6 className="font-semibold mt-1">Welcome</h6>
                      <p className="text-sm my-3 leading-relaxed text-gray-700">
                        <b>
                          What clients normally ask is how much money and time
                          it costs to develop the e-learning.
                        </b>
                        This is a tricky part because the length of development
                        time depends on the client's organization, the
                        interactivity and the length of the course. You could
                        argue it's case by case. However,
                        <span className="primary-color underline">
                          Bryan Chapman
                        </span>
                        has done a research back to 2010 on how much time and
                        money it costs to develop a new e-learning based on a
                        certain level.
                      </p>
                      <span className="block border-b-4 rounded border-[#14CDA1] w-28 mt-3 mx-auto my-4"></span>
                      <h6 className="text-gray-500 text-center mb-1 font-light">
                        There are 3 levels of e-learning in terms of
                        interactivity,
                      </h6>
                      <ol className="list-decimal list-inside text-sm text-gray-700 leading-loose">
                        <li>
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit ut aliquam, purus sit amet luctus venenatis,
                          lectus magna fringilla urna, porttitor rhoncus dolor
                          purus non enim praesent
                        </li>
                        <li>
                          elementum facilisis leo, vel fringilla est ullamcorper
                          eget nulla facilisi etiam dignissim diam quis enim
                          lobortis scelerisque fermentum dui faucibus in ornare
                          quam viverra orci
                        </li>
                        <li>
                          sagittis eu volutpat odio facilisis mauris sit amet
                          massa vitae tortor condimentum lacinia
                        </li>
                      </ol>
                    </div>
                  </div>
                  <div className="relative mt-4 h-10 flex items-center">
                    <span className="block border-b border-[#14CDA1] w-full "></span>
                    <button className="absolute left-1/2 w-10 h-10 text-white bg-[#14CDA1] transition-colors duration-150 rounded-full focus:shadow-outline ">
                      2
                    </button>
                  </div>
                </div>

                {/* Third Nav content */}
                <div id="#3" className="mt-4">
                  <div className="mb-6">
                    <div className="ml-2 mb-6">
                      <h5 className="font-semibold">
                        3. Create a course landing page{" "}
                      </h5>
                      <span className="block border-b-2 border-[#14CDA1] w-28 mt-3"></span>
                    </div>
                    <Image src={image3} />
                    <div className="mb-2">
                      <p className="text-sm my-3 leading-relaxed text-gray-700">
                        What clients normally ask is how much money and time it
                        costs to develop the e-learning. This is a tricky part
                        because the length of development time depends on the
                        client's organization, the interactivity and the length
                        of the course. You could argue it's case by case.
                        However, Bryan Chapman has done a research back to 2010
                        on how much time and money it costs to develop a new
                        e-learning based on a certain level.
                      </p>
                      <div className="flex gap-10 mb-3">
                        <p className="basis-1/2 text-sm leading-loose text-gray-700">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit ut aliquam, purus sit amet luctus venenatis,
                          lectus magna fringilla urna, porttitor rhoncus dolor
                          purus non enim praesent elementum
                          <b> facilisis leo, vel fringilla</b> est ullamcorper
                          eget nulla facilisi etiam dignissim diam quis enim
                          lobortis scelerisque enim praesent elementum facilisis
                          leo, vel fringilla est ullamcorper eget nulla facilisi
                        </p>
                        <div className="basis-1/2">
                          <Image src={image4} />
                        </div>
                      </div>
                      <div className="flex gap-10 ">
                        <div className="basis-1/2">
                          <Image src={image5} />
                        </div>
                        <p className="basis-1/2 text-sm leading-loose text-gray-700">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit ut aliquam, purus sit amet
                          <b>
                            luctus venenatis, lectus magna fringilla urna,
                            porttitor rhoncus dolor purus non
                          </b>
                          enim praesent elementum facilisis leo, vel fringilla
                          est ullamcorper eget nulla facilisi etiam dignissim
                          diam quis enim lobortis scelerisque enim praesent
                          elementum facilisis leo, vel fringilla est ullamcorper
                          eget nulla
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="relative mt-4 h-10 flex items-center">
                    <span className="block border-b border-[#14CDA1] w-full "></span>
                    <button className="absolute left-1/2 w-10 h-10 text-white bg-[#14CDA1] transition-colors duration-150 rounded-full focus:shadow-outline ">
                      3
                    </button>
                  </div>
                </div>

                {/* Fourth Nav content */}
                <div id="#3" className="mt-4">
                  <div className="mb-6">
                    <div className="ml-2 mb-6">
                      <h5 className="font-semibold">4. Create a Storyboard</h5>
                      <span className="block border-b-2 border-[#14CDA1] w-28 mt-3"></span>
                    </div>
                    <div className="flex gap-10 mb-6">
                      <div className="basis-1/2">
                        <Image src={image6} />
                      </div>
                      <div className="basis-1/2 py-8 my-auto">
                        <h6 className="text-md font-semibold">
                          Writing Learning Objectives
                        </h6>
                        <p className="text-sm my-4 leading-relaxed text-gray-700">
                          What clients normally ask is how much money and time
                          it costs to develop the e-learning. This is a tricky
                          part because the length of development time depends on
                          the client's organization, t
                        </p>
                        <span className="text-xs text-blue-600 underline">
                          https://learning-objectives.easygenerator.com/
                        </span>
                      </div>
                    </div>
                    <div className="flex gap-10 mb-6">
                      <div className="basis-1/2 py-8 my-auto">
                        <h6 className="text-md font-semibold">
                          Building Course Outline
                        </h6>
                        <p className="text-md my-8 leading-loose text-gray-700">
                          Following some suggestions below to structure a course
                          clearly.
                        </p>
                        <span className="text-xs text-blue-600 underline whitespace-nowrap">
                          https://teach.udemy.com/course-creation/curriculum/
                        </span>
                      </div>
                      <div className="basis-1/2">
                        <Image src={image7} />
                      </div>
                    </div>
                    <p className="text-sm my-4 leading-loose text-gray-700">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit ut
                      aliquam, purus sit amet luctus venenatis, lectus magna
                      fringilla urna, porttitor rhoncus dolor purus non enim
                      praesent elementum facilisis leo, vel fringilla est
                      ullamcorper eget nulla facilisi etiam dignissim diam quis
                      enim lobortis scelerisque
                    </p>
                    <div className="flex gap-10">
                      <Image src={image8} />
                      <Image src={image9} />
                    </div>
                    <p className="text-sm my-4 leading-loose text-gray-700">
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit ut
                      aliquam, purus sit amet luctus venenatis, lectus magna
                      fringilla urna, porttitor rhoncus dolor purus non enim
                      praesent elementum facilisis leo, vel fringilla est
                      ullamcorper eget nulla facilisi etiam dignissim diam quis
                      enim lobortis scelerisque
                    </p>
                  </div>
                  <div className="relative mt-4 h-10 flex items-center">
                    <span className="block border-b border-[#14CDA1] w-full "></span>
                    <button className="absolute left-1/2 w-10 h-10 text-white bg-[#14CDA1] transition-colors duration-150 rounded-full focus:shadow-outline ">
                      4
                    </button>
                  </div>
                </div>

                {/* Tips goes here */}
                <div className="px-5">
                  <div className="border-t py-3  my-8">
                    <h3 className="text-2xl font-semibold my-2">Tips</h3>
                    <div className="flex flex-col gap-3">
                      {/*  */}
                      {dummyData.map((d, i) => (
                        <div
                          key={i}
                          className="relative w-full flex items-center justify-between bg-[#F3F3F3] px-4 py-3 rounded "
                        >
                          <span className="w-2 h-full bg-[#14CDA1] rounded-l absolute left-0"></span>
                          <div className="text-gray-500 text-xs ml-2">
                            <b>Tip:</b> Lorem ipsum dolor sit amet, consectetur
                            adipiscing elit. Id amet libero cursus nec dis quis
                            at egesllis cras gravida nulla maecenas et.
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>

                {/* Comments goes here */}
                <div className="mt-10">
                  <h2>Ask Question?</h2>
                  {/* Comment Input */}
                  <div className="flex flex-col mt-3">
                    <div className="flex h-8 gap-3">
                      <div className="w-10 h-10">
                        <Image src={profile} width={32} height={32} className=" rounded-full " />
                      </div>
                      <input
                        className="appearance-none border rounded-lg w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline placeholder:text-gray-200 placeholder:text-xs"
                        id="comment"
                        type="text"
                        placeholder="Join the discussion"
                      />
                      <button
                        className="whitespace-nowrap bg-sky-500 hover:bg-sky-400 focus:shadow-outline focus:outline-none text-white py-1 px-3 rounded "
                        type="button"
                      >
                        Ask question
                      </button>
                    </div>
                    <div></div>
                  </div>
                  {/* Comments */}
                  {dummyData.map((d)=>(
                  <div className="flex flex-col mt-5 pb-3 border-b">
                    <div className="flex items-center gap-3 h-14">
                      <div className="">
                        <Image src={profile} width={32} height={32} className=" rounded-full" />
                      </div>
                      <div className="flex justify-between w-full">
                        <div>
                          <h5 className="text-xl mb-1">{guide.Author}</h5>
                          <p className="text-gray-400 text-xs">What we choose not to say is just as important as what we decide to say</p>
                        </div>
                        <span className="text-gray-400 text-xs ">14 July, 2020</span>
                      </div>
                    </div>
                    <div></div>
                  </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </>
      )}
      {/* for you section goes here */}
      <Slider
        sliderName="guides"
        sliderExploreName={"Just For You"}
        data={guides}
        row={false}
      />
      {/* search section  */}
      <div className="max-w-5xl mx-auto md:py-10 px-4 xl:px-0 2xl:max-w-screen-lg">
        <Search />
      </div>
    </div>
  );
};
export default GuideDetailed;
