import React from "react";
import Image from "next/image";
import Link from "next/link";
import Head from "next/head";
import moment from "moment";
import ReactPlayer from "react-player";
import { useRouter } from "next/router";
import Search from "../../Components/Search/Search";
import Loader from "../../Components/General Components/Loader";
import StyledButton1 from "./../../Components/General Components/StyledButton1";
import ReviewProgressBar from "../../Components/Reviews/ReviewProgressBar";
import ReviewProgressCircle from "../../Components/Reviews/ReviewProgressCircle";
import RatingStars from "../../Components/Reviews/RatingStars";
import RatingBars from "../../Components/Reviews/RatingBars";
import ReviewCard from "./../../Components/Reviews/ReviewCard";
import ReviewComment from "../../Components/Reviews/ReviewComment";

import { reviews } from "../../utils/ReviewsData";
import profileImage from "../../assets/images/profile.jpg";
import logo from "../../assets/images/logo/logo_icon_green.png";
import noimage from "../../assets/images/no-image.png";
import amazon from "../../assets/images/companies/amazon-logo.png";
import bellroy from "../../assets/images/companies/bellroy.png";
import ad from "../../assets/images/daypackad.jpg";

const ReviewDetailed = () => {
  const router = useRouter();
  if (router.isFallback) {
    return <Loader />;
  }
  //   filtering reviewsData according to the slug recieved
  const detailedReview = reviews.filter(
    (review) => review.slug === router.query.slug
  );
  const review = detailedReview[0];

  const dummyData = ["", "", "", ""];

  return (
    <div className="pt-20 mx-auto max-w-8xl">
      <Head>
        <title>Reviews</title>
        <link rel="icon" href="/favicon.png" />
      </Head>
      <div className="container mx-auto px-40 mb-8 pt-4">
        {/* Breadcrumb */}
        <nav className="rounded w-full mb-4">
          <ol className="list-reset flex">
            <li>
              <a href="#" className="text-gray-300">
                Reviews
              </a>
            </li>
            <li>
              <span className="mx-2 text-gray-300">{`>`}</span>
            </li>
            <li>
              <a href="#" className="text-gray-300">
                Daypacks
              </a>
            </li>
            <li>
              <span className="mx-2 text-gray-300">{`>`}</span>
            </li>
            <li className="text-gray-500">Daypack by bellory</li>
          </ol>
        </nav>
        {review && (
          <div className="grid grid-cols-1 lg:grid-cols-12 gap-12 py-6">
            {/* Review details goes here ... */}
            <div className="lg:col-span-8 col-span-1">
              {/* first part  */}
              <div>
                <h1 className="text-4xl">{review.title}</h1>
                <p className="my-4 text-gray-500">{review.excerpt}</p>
                <StyledButton1
                  cardTitle="Check Buying Options"
                  classNameProp="lg:w-60 rounded"
                />
                <ReactPlayer
                  className="mt-8 "
                  width="100%"
                  light={true}
                  url={"https://www.youtube.com/watch?v=QgMCod3dfRU"}
                  playing
                />
              </div>
              {/* Verdict part */}
              <div className="border border-gray-100 mt-12 shadow-sm relative">
                <div className="w-full bg-black text-white text-3xl py-2 px-4">
                  <span>Our Verdict</span>
                </div>
                <div className="bg-primary-color md:rounded  text-white text-center md:absolute w-20 flex md:flex-col md:justify-center right-2 top-3 shadow-lg">
                  <span className="border-b border-white py-2 px-2">
                    <span className="font-bold text-3xl">7.6</span><span className="text-xs">/10</span>
                  </span>
                  <span className="text-md font-semibold">Good</span>
                </div>
                <div className="px-8 mb-4">
                  <div className="pt-12 ">
                    <div className="grid grid-cols-1 gap-4">
                      <ReviewProgressBar name="Form" progress="80" />
                      <ReviewProgressBar name="Design" progress="75" />
                      <ReviewProgressBar name="Value" progress="66" />
                    </div>
                    <div className="grid grid-cols-1 lg:grid-cols-2 gap-4 mt-6 relative">
                      <div className="col-span-1">
                        <h1 className="text-3xl font-semibold">Pros</h1>
                        <ul className="list-disc list-inside mt-3 text-gray-500">
                          <li>
                            Lorem ipsum dolor sit amet, isaa consectetur elit.
                          </li>
                          <li>
                            Lorem ipsum dolor sit amet, isaa consectetur elit.
                          </li>
                          <li>
                            Lorem ipsum dolor sit amet, isaa consectetur elit.
                          </li>
                        </ul>
                      </div>
                      <span className="hidden lg:block absolute h-full border-r border-gray-200 mr-2 right-1/2"></span>
                      <div className="col-span-1">
                        <h1 className="text-3xl font-semibold">Cons</h1>
                        <ul className="list-disc list-inside mt-3 text-gray-500">
                          <li>
                            Lorem ipsum dolor sit amet, isaa consectetur elit.
                          </li>
                          <li>
                            Lorem ipsum dolor sit amet, isaa consectetur elit.
                          </li>
                          <li>
                            Lorem ipsum dolor sit amet, isaa consectetur elit.
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="px-2 py-2">
                  <StyledButton1
                    classNameProp="rounded "
                    cardTitle="Check Buying Options"
                  />
                </div>
              </div>
              {/* Technical Details part */}
              <div className="border border-gray-100 mt-12 shadow-sm">
                <div className="w-full bg-black text-white text-3xl py-2 px-4">
                  <span>Technical Details</span>
                </div>
                <div className="px-4 mb-4">
                  <div className="pt-8 px-4 mb-4 ">
                    <div className="grid grid-cols-2 gap-8">
                      <div className="text-center">
                        <ReviewProgressCircle progress={90} />
                        <h5 className="font-semibold text-lg">
                          Carry-On Compliance
                        </h5>
                        <p className="primary-color text-sm">
                          View 131/144 Airlines
                        </p>
                      </div>
                      <div className="text-center">
                        <ReviewProgressCircle progress={56} />
                        <h5 className="font-semibold text-lg">Like the Look</h5>
                        <p className="primary-color text-sm">
                          Polled on Instagram
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="mt-8">
                    <div className="grid grid-cols-2 border-b border-gray-200 py-3">
                      <div>
                        <h6 className="font-semibold">Capacity</h6>
                        <p className="text-gray-500">16L</p>
                      </div>
                      <div className="border-l border-gray-200 pl-3">
                        <h6 className="font-semibold">Weight (lb)</h6>
                        <p className="text-gray-500">1.43 lb (0.6 kg)</p>
                      </div>
                    </div>
                    <div className="grid grid-cols-2 border-b border-gray-200 py-3">
                      <div>
                        <h6 className="font-semibold">Manufacturing Country</h6>
                        <p className="text-gray-500">Philippines</p>
                      </div>
                      <div className="border-l border-gray-200 pl-3">
                        <h6 className="font-semibold">
                          Laptop Compartment Size
                        </h6>
                        <p className="text-gray-500">13"</p>
                      </div>
                    </div>
                    <div className="grid grid-cols-1 border-b border-gray-200 py-3">
                      <div>
                        <h6 className="font-semibold">Dimensions</h6>
                        <p className="text-gray-500">
                          17.3 in x 11.8 in x 6.3 in (43.9 x 30 x 16 cm)
                        </p>
                      </div>
                    </div>
                    <div className="grid grid-cols-1 border-b border-gray-200 py-3">
                      <div>
                        <h6 className="font-semibold">Warranty Information</h6>
                        <p className="primary-color ">
                          Bellroy 3 Year Warranty
                        </p>
                      </div>
                    </div>
                    <div className="grid grid-cols-1 py-3">
                      <div>
                        <h6 className="font-semibold">Notable Materials</h6>
                        <p className="text-gray-500">
                          Recycled Nylon, Polyester, Leather, YKK Zippers,
                          Duraflex Hardware, Fidlock Hardware, Woojin Hardware
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* Buying options */}
              <div className="border border-gray-100 mt-12 shadow-md">
                <div className="w-full bg-black text-white text-3xl py-2 px-4">
                  <span>Buying Options</span>
                </div>
                <div className="flex flex-col">
                  <div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                    <div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                      <div className="overflow-hidden">
                        <table className="min-w-full divide-y divide-gray-200">
                          <thead className="bg-white">
                            <tr>
                              <th
                                scope="col"
                                className="pl-6 py-3 text-left text-md font-semibold text-black tracking-wider"
                              >
                                Brand
                              </th>
                              <th
                                scope="col"
                                className="pl-6 py-3 text-left text-md font-semibold text-black tracking-wider"
                              >
                                Price
                              </th>
                              <th
                                scope="col"
                                className="pr-6 py-3 text-left text-md font-semibold text-black tracking-wider"
                              >
                                Deal
                              </th>
                              <th scope="col" className="relative px-6 py-3">
                                <span className="sr-only">Action</span>
                              </th>
                            </tr>
                          </thead>
                          <tbody className="bg-white">
                            <tr className="bg-gray-200">
                              <td className="pl-6 py-4 whitespace-nowrap">
                                <Image
                                  className="h-10 w-10 rounded-full"
                                  height={50}
                                  width={90}
                                  src={amazon}
                                  alt="amazon"
                                />
                              </td>
                              <td className="pl-6 py-4 whitespace-nowrap">
                                <div className="text-sm text-gray-900">
                                  $129,00
                                </div>
                              </td>
                              <td className="pr-6 py-4 whitespace-nowrap">
                                <div className="text-sm text-gray-900">--</div>
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <a
                                  href="#"
                                  className="text-indigo-600 hover:text-indigo-900"
                                >
                                  <StyledButton1
                                    cardTitle="Buy"
                                    classNameProp="w-20 rounded text-md font-semibold"
                                  />
                                </a>
                              </td>
                            </tr>
                            <tr className="bg-white">
                              <td className="px-6 py-4 whitespace-nowrap">
                                <Image
                                  className="h-10 w-10 rounded-full"
                                  height={50}
                                  width={60}
                                  src={bellroy}
                                  alt="bellroy"
                                />
                              </td>
                              <td className="pl-6 py-4 whitespace-nowrap">
                                <div className="text-sm text-gray-900">--</div>
                              </td>
                              <td className="pr-6 py-4 whitespace-nowrap">
                                <div className="text-sm text-gray-900">--</div>
                              </td>
                              <td className="px-6 py-4 whitespace-nowrap text-right text-sm font-medium">
                                <a
                                  href="#"
                                  className="text-indigo-600 hover:text-indigo-900"
                                >
                                  <StyledButton1
                                    cardTitle="Buy"
                                    classNameProp="w-20 rounded text-md font-semibold"
                                  />
                                </a>
                              </td>
                            </tr>

                            {/* <!-- More people... --> */}
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              {/* Review in detail here */}
              <div className="my-12">
                <h1>Review in Detail</h1>
                {dummyData.map((d) => (
                  <div className="py-4 mb-4">
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Ut lacus justo, pellentesque eleifend libero at. Orci,
                      enim, blandit gravida condimentum ac potenti. Rhoncus,
                      diam vitae sed semper. Volutpat elementum turpis eget
                      sollicitudin non, vel enim nunc. Dictum ut nunc, et eu
                      varius at id eget. Blandit non imperdiet sit ultricies
                      metus eget mollis. Morbi blandit lectus enim diam ac.
                    </p>
                    <div className="my-4">
                      <Image src={noimage} />
                    </div>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Ut us justoas, pellentesque eleifend libero at., blandit
                      gravida condime ntum ac potenti. Rhoncus, diam vitae sed
                      semper. Volutpat sasdas dsdelementum turpis eget
                      sollicitudin non, vel enim nunc. Dictum ut nunc, et eu
                      varius at id eget. Blandit non imperdiet sit ultricies
                      metus eget mollis. Morbi blandit lectus enim diam ac.
                    </p>
                    <h1 className="my-4 text-4xl font-semibold">
                      Materials and Aesthetic
                    </h1>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                      Ut lacus justo, pellentesque eleifend libero at. Orci,
                      enim, blandit gravida condimentum ac potenti. Rhoncus,
                      diam vitae sed semper. Volutpat elementum turpis eget
                      sollicitudin non, vel enim nunc. Dictum ut nunc, et eu
                      varius at id eget. Blandit non imperdiet sit ultricies
                      metus eget mollis. Morbi blandit lectus enim diam ac.
                    </p>
                  </div>
                ))}
                <StyledButton1
                  cardTitle="Check Buying Options"
                  classNameProp="lg:w-60 rounded"
                />
              </div>

              {/* profile section */}
              <div className="border-y border-gray-200 py-4">
                <div className="flex-row gap-4 flex justify-start items-center">
                  <Link href="#">
                    <a className="flex-shrink-0">
                      <Image
                        alt="profil"
                        src={profileImage}
                        height={50}
                        width={50}
                        className="mx-auto object-cover rounded-full"
                      />
                    </a>
                  </Link>
                  <div className=" flex flex-col">
                    <span className="text-black text-lg font-semibold">
                      By BTR Islam
                    </span>
                    <span className="text-gray-400 text-xs">
                      Created
                      <span className="ml-1">
                        {moment(review.Date).format("Do MMM YY")}
                      </span>
                    </span>
                  </div>
                </div>
              </div>

              {/* Community reviews Section */}
              <div className="pt-8 border-y border-gray-200">
                <div className="flex justify-between items-center">
                  <div className="flex items-center">
                    <Link href="#">
                      <a className="block flex-shrink-0 mt-2">
                        <Image
                          alt="profil"
                          src={logo}
                          height={40}
                          width={40}
                          className="mx-auto object-cover rounded-full bg-black"
                        />
                      </a>
                    </Link>
                    <h2 className="text-3xl font-semibold ml-3">
                      Community Reviews
                    </h2>
                  </div>
                  <button className="w-40 text-white py-2 font-semibold shadow-lg">
                    Write Review
                  </button>
                </div>
                <div className="mt-8 px-6">
                  <div>
                    <RatingStars
                      value={review.Rating}
                      text={review.numReviews}
                    />
                  </div>
                  <div>
                    <RatingBars />
                  </div>
                </div>
              </div>

              {/* Comments section */}
              <div className="pb-8 ">
                <ReviewComment />
                <ReviewComment />
                <ReviewComment />
              </div>
            </div>
            <div className="lg:col-span-4 col-span-1">
              <div className="hidden lg:block relative top-8">
                <div className="mt-40">
                  <Image src={ad} />
                  <StyledButton1 cardTitle="Check Buying Options" classNameProp="m-0" />
                </div>
                <p className="mt-10">Ads goes here ...</p>
              </div>
            </div>

            {/* Show more button */}
            <div className="text-center -mt-20 pt-6 lg:col-span-12 col-span-1 border-t border-gray-300">
              <button className="bg-white hover:bg-gray-500 text-gray-800 font-medium hover:text-white py-2 md:px-4 md:border border-gray-200 hover:border-transparent rounded-full h-11 transition ease-in-out duration-200 hover:scale-105">
                Show more
              </button>
            </div>
          </div>
        )}
        {/* Other reviews goes here */}
        <div className="">
          <h1 className="text-3xl font-semibold">Other Reviews</h1>
          <div className=" mx-auto grid grid-cols-1 lg:grid-cols-12 ">
            {reviews.map((r, i) => (
              <div className="col-span-4">
                <ReviewCard data={r} key={i} />
              </div>
            ))}
            {reviews.map((r, i) => (
              <div key={i} className="col-span-4">
                <ReviewCard data={r} />
              </div>
            ))}
          </div>
          <div className="text-center pt-6 lg:col-span-12 col-span-1 border-t border-gray-300">
            <button className="bg-white hover:bg-gray-500 text-gray-800 font-medium hover:text-white py-2 md:px-4 md:border border-gray-200 hover:border-transparent rounded-full h-11 transition ease-in-out duration-200 hover:scale-105">
              Show all reviews
            </button>
          </div>
        </div>
      </div>
      {/* search section  */}
      <div className="max-w-5xl mx-auto md:py-10 px-4 xl:px-0 2xl:max-w-screen-lg">
        <Search />
      </div>
    </div>
  );
};
export default ReviewDetailed;
