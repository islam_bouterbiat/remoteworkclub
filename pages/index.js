import Head from "next/head";
import Image from "next/image";
import Search from "../Components/Search/Search";
import ReviewCard from "../Components/Reviews/ReviewCard";
import GuideCard from "../Components/Guides/GuideCard";
import ExploreBar from "../Components/General Components/ExploreBar";
import InspirationCard from "../Components/Inspirations/InspirationCard";
import BlogCard from "../Components/Blogs/BlogCard";
import Slider from "../Components/General Components/Slider";

import hero from "../assets/images/hero-img.png";
import { companies } from "./../utils/companiesImgData";
import { reviews } from "../utils/ReviewsData";
import { guides } from "../utils/guidesData";
import { inspirations } from "../utils/inspirationsData";
import { blogs } from "../utils/blogsData";

export default function Home() {
  return (
    <div className="pt-20 mx-auto max-w-8xl">
      <Head>
        <title>Remote Work Club</title>
        <link rel="icon" href="/favicon.png" />
      </Head>
      {/* hero part */}
      <div className="hero-section mx-auto w-full md:px-48">
        <div className="flex flex-col md:flex-row justify-center items-center max-w-7xl mx-auto pt-10 pb-5 px-4 xl:px-0 gap-10">
          <div className="ml-2">
            <h1 className="font-medium mb-6 text-4xl md:text-4xl lg:text-5xl leading-10">
              Be free.
              <br />
              Be part of the
              <br />
              Remote Work Club
            </h1>
            <p
              className="text-lg md:text-xl font-light text-gray-500 md:whitespace-pre-line"
              style={{ maxWidth: "40ch" }}
            >
              Find remote jobs that offer you the freedom {""}
              to work remotely from home or anywhere around the world.
            </p>
            <div className="flex md:flex-row flex-col mt-10 mx-auto w-72 md:w-auto">
              <button className="text-white font-bold py-3 px-10 rounded md:mr-5 mb-2">
                Post a job
              </button>
              <button className="text-emerald-400 hover:text-white bg-transparent font-bold mb-2 py-2 px-10 rounded outline outline-1">
                Find a job
              </button>
            </div>
          </div>
          <div className="hidden lg:block ">
            <Image src={hero} height={543} width={620} alt="Remote Work Club" />
          </div>
        </div>
      </div>
      {/* companies part */}
      <div className="max-w-7xl mx-auto py-10 px-4 xl:px-0 opacity-50">
        <div className="flex flex-col items-center text-center">
          <h5 className="text-gray-500 font-normal text-lg md:text-xl mb-4">
            Trusted by the world's leading companies
          </h5>
          <div className="flex justify-center flex-wrap items-center gap-5 md:gap-20">
            {companies.map((c, index) => (
              <Image
                className="filter grayscale"
                src={c.imgUrl}
                alt={c.name}
                key={index}
              />
            ))}
          </div>
        </div>
      </div>
      <div className="md:px-20 mt-6 overflow-visible">
        {/* sliders Section */}
        <Slider
          sliderName="reviews"
          sliderExploreName="Reviews"
          data={reviews}
          row={true}
        />
        <Slider
          sliderName="guides"
          sliderExploreName="Guides"
          data={guides}
          row={true}
        />
        <Slider
          sliderName="inspirations"
          sliderExploreName="Inspirations"
          data={inspirations}
          row={true}
        />
        {/* Blogs Section */}
        <Slider
          sliderName="blogs"
          sliderExploreName={"Blogs"}
          data={blogs}
          row={false}
        />
        {/* Search Section */}
        <div className="max-w-5xl mx-auto md:py-10 px-4 xl:px-0 2xl:max-w-screen-lg">
          <Search />
        </div>
      </div>
    </div>
  );
}
