module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./Components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      spacing: {
        "-3px": "-3px",
        0.32: "-15px",
        48: "13rem",
        "40ch": "40ch",
        "15px": "15px",
        "20px": "20px",
        "30px": "30px",
        "150px": "150px",
        "200px": "200px",
        "100vw": "100vw",
        "450px": "450px",
      },
      colors: {
        primary: "#14cda1",
        secondary: "#161718",
        third: "#fd8249",
      },
      margin: {
        "08": "-0.08rem",
      },
    },
  },

  plugins: [],
};
