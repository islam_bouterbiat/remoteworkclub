import React, { useState } from "react";
import Image from "next/image";
import Link from "next/link";
import {
  AiFillFacebook,
  AiFillInstagram,
  AiFillTwitterCircle,
} from "react-icons/ai";

import logo from "../../assets/images/logo.png";
import { footerData } from "../../utils/footerData";
import FooterMenu from "./FooterMenu";

const Footer = () => {
  return (
    <div className="footer shadow-sm w-full bg-black w-screen md:px-20 relative">
      <div className="max-w-7xl mx-auto pt-1 md:pt-10 px-4 xl:px-0 max-w-screen-lg 2xl:max-w-screen-xl">
        <div className="flex flex-col md:grid grid-cols-6 w-full pb-28 md:pb-10 border-b border-white border-opacity-5">
          {/* <!-- Logo --> */}
          <div className="absolute md:relative bottom-0 left-0 md:float-left md:block transition ease-in-out duration-300 hover:scale-105">
            <Link href="/" passHref>
              <a className="relative left-4 bottom-4 lg:static">
                <Image src={logo} width={120} height={45} alt="RWC" />
              </a>
            </Link>
          </div>
          {/* <!-- Nav --> */}
          {footerData.map((d, index) => (
            <div key={index} className="widget md:p-0 ">
              <FooterMenu data={d} />
            </div>
          ))}
        </div>

        <div className="flex flex-row items-center justify-between py-1 md:py-6">
          <span className="absolute bottom-4 right-4 md:static text-white text-opacity-60">
            Copyright © 2022
          </span>
          <div className="flex md:static absolute bottom-12 right-2">
            <a href="https://www.google.com/" className="socialLink">
              <AiFillFacebook size="2rem" />
            </a>
            <a href="https://www.google.com/" className="socialLink">
              <AiFillInstagram size="2rem" />
            </a>
            <a href="https://www.google.com/" className="socialLink">
              <AiFillTwitterCircle size="2rem" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
