import React, { useState, useEffect } from "react";
import { RiArrowDropUpLine, RiArrowDropDownLine } from "react-icons/ri";

const FooterMenu = ({ data }) => {
  // initialize the size of screen (undefined because of the fucking window is undefined problem)
  const [size, setSize] = useState({
    width: undefined,
    height: undefined,
  });
  // then here we set the size of the screen size inside of useEffect Hook to prevent the fucking window error
  useEffect(() => {
    setSize({ width: window.innerWidth, height: window.innerHeight });
  }, []);
  //setting the subFooterNavigations useState hook to true or false according to the reloading screen size
  const [subFooterNav, setSubFooterNnav] = useState(
    size.width > 768 ? true : false
  );
  //show and hide the subNav
  const showSubFooterNav = () => setSubFooterNnav(!subFooterNav);
  //hundling the size on screen resize
  useEffect(() => {
    const handleResize = () => {
      setSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };
    window.addEventListener("resize", handleResize);

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  //open the footer SubNav on large screen (when we go from mobile to desktop)
  useEffect(() => {
    if (size.width > 768 && subFooterNav === false) {
      setSubFooterNnav(true);
    }
  }, [size.width, subFooterNav]);

  return (
    <>
      <div
        className="flex justify-between items-center py-3 md:border-none border-b border-[#2C2C2C]"
        onClick={data.content && showSubFooterNav}
      >
        <h3 className="text-sm text-white md:p-0 md:mb-4">{data.name}</h3>
        {data.content && subFooterNav ? (
          <RiArrowDropUpLine size={28} className="md:hidden text-white" />
        ) : (
          <RiArrowDropDownLine size={28} className="md:hidden text-white" />
        )}
      </div>
      <div className="bg-[#181818] md:bg-transparent px-3">
        {subFooterNav &&
          data.content.map((c, i) => (
            <div key={i} className="flex flex-col justify-center   gap-4 py-3">
              <a
                href="#"
                className="text-white md:text-gray-500 text-xs hover:text-white transition ease-in-out duration-200 hover:scale-105"
              >
                {c}
              </a>
            </div>
          ))}
      </div>
    </>
  );
};

export default FooterMenu;
