import React from "react";

const SearchBar = () => {
  return (
    <div className="search-field relative border border-gray-200 hover:border-gray-400 rounded-md overflow-hidden duration-75">
      <svg
        width="24"
        height="24"
        fill="none"
        aria-hidden="true"
        className="mr-3 flex-none text-gray-400 absolute top-4 left-4 pointer-events-none"
      >
        <path
          d="m19 19-3.5-3.5"
          stroke="currentColor"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        ></path>
        <circle
          cx="11"
          cy="11"
          r="6"
          stroke="currentColor"
          strokeWidth="2"
          strokeLinecap="round"
          strokeLinejoin="round"
        ></circle>
      </svg>
      <input
        type="text"
        className="w-full p-4 focus:outline-none pl-12 placeholder:text-gray-200"
        placeholder="Remote job, Blog, Inspirations..."
      />
      <button className="hidden md:block text-white font-light py-1 px-10 rounded absolute top-3 right-4">
        Search
      </button>
    </div>
  );
};

export default SearchBar;
