import React from "react";
import Link from "next/link";
import { RiArrowRightSLine } from "react-icons/ri";
import { MdOutlineDoubleArrow } from "react-icons/md";
import SearchBar from "./SearchBar";
import DoubleArrowIcon from "@mui/icons-material/DoubleArrow";

const suggestions = [
  { name: "RWC Review" },
  { name: "Success Blog" },
  { name: "Inspiration" },
  { name: "Best Deal" },
  { name: "Skilled Employee" },
  { name: "Remote Work Resources" },
];

const Search = () => {
  return (
    <div className='search-box text-center md:px-20'>
      <h2 className='mb-8 text-3xl font-medium'>
        Let's dive into ocean of knowledge...{" "}
      </h2>
      <SearchBar />
      <div className='popular-searches flex gap-1 justify-center items-center py-6'>
        <span className='text-sm text-gray-400 whitespace-nowrap'>
          Popular Searches
        </span>
        <span>
          <DoubleArrowIcon className='text-gray-400 w-15px' />
        </span>
        <div className='hidden suggestions md:flex flex-wrap gap-5 px-10'>
          {suggestions.map((s, index) => (
            <div
              className='suggestion-link text-xs font-medium hover:underline whitespace-nowrap'
              key={index}>
              <Link href='#'>{s.name}</Link>
            </div>
          ))}
        </div>
        <a href='#'>
          <div className='hidden md:flex items-center justify-center text-gray-700'>
            <span className=' text-xs text-gray-400'>More</span>
            <RiArrowRightSLine size={14} className='mt-1 text-gray-400' />
          </div>
        </a>
      </div>
    </div>
  );
};

export default Search;
