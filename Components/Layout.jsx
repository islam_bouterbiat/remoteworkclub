import React from "react";
import NavBar from "./General Components/NavBar";
import Footer from "./footer/Footer";

const Layout = ({ children }) => (
  <div>
    <NavBar />
    {children}
    <Footer />
  </div>
);

export default Layout;
