import React from "react";
import { IoIosStarHalf, IoIosStarOutline, IoIosStar } from "react-icons/io";

const RatingStars = ({ text, value, color }) => {
  const Rvalue = value / 2;
  return (
    <>
      <div className="flex items-center gap-2">
        <span style={{ color }}>
          {Rvalue >= 1 ? (
            <IoIosStar size={22} />
          ) : Rvalue >= 0.5 ? (
            <IoIosStarHalf size={22} />
          ) : (
            <IoIosStarOutline size={22} />
          )}
        </span>
        <span style={{ color }}>
          {Rvalue >= 2 ? (
            <IoIosStar size={22} />
          ) : Rvalue >= 1.5 ? (
            <IoIosStarHalf size={22} />
          ) : (
            <IoIosStarOutline size={22} />
          )}
        </span>
        <span style={{ color }}>
          {Rvalue >= 3 ? (
            <IoIosStar size={22} />
          ) : Rvalue >= 2.5 ? (
            <IoIosStarHalf size={22} />
          ) : (
            <IoIosStarOutline size={22} />
          )}
        </span>
        <span style={{ color }}>
          {Rvalue >= 4 ? (
            <IoIosStar size={22} />
          ) : Rvalue >= 3.5 ? (
            <IoIosStarHalf size={22} />
          ) : (
            <IoIosStarOutline size={22} />
          )}
        </span>
        <span style={{ color }}>
          {Rvalue >= 5 ? (
            <IoIosStar size={22} />
          ) : Rvalue >= 4.5 ? (
            <IoIosStarHalf size={22} />
          ) : (
            <IoIosStarOutline size={22} />
          )}
        </span>
        {text && (
          <span className="ml-3 text-xl font-semibold mt-1">
            {Rvalue} out of 5
          </span>
        )}
      </div>
      {text && (
        <div className="text-md text-gray-600 mt-2 ml-1">
          {text && text} global ratings
        </div>
      )}
    </>
  );
};

RatingStars.defaultProps = {
  color: "#5EC9A3",
};

export default RatingStars;
