import React from "react";
import Image from "next/image";
import Link from "next/link";
import { BsFillPeopleFill } from "react-icons/bs";

import image from "../../assets/images/daypack.jpg";
import logo_icon from "../../assets/images/logo/logo_icon_green.png";
import community_icon from "../../assets/images/community_icon.png";

import StyledButton1 from "../General Components/StyledButton1";

const ReviewCard = ({
  data: { title, excerpt, Tags, imageUrl, Rating, Author, slug },
}) => {
  const rating = Rating.toFixed(1);

  return (
    <div className="py-10 px-2 relative grid place-content-center flex-none snap-center ">
      <div className="max-w-xs overflow-visible shadow-md grid h-450px">
        <div className="relative">
          <Image src={image} alt={title} />

          <span className="inline-flex absolute bottom-4 right-4 items-center justify-center py-2 px-1 text-xl font-bold leading-none text-white rating-review-badge">
            {rating}
            <span className="text-xs float-bottom mt-1">/10</span>
          </span>
        </div>
        <span className="absolute px-2 p-0.5 font-bold top-11 -left-3px review-card-cat-badge text-white z-50 shadow-md">
          Gears
        </span>
        <div className="p-4">
          <div className="flex gap-4">
            <div className="font-bold text-xl mb-2 col-span-1">{title}</div>
            <div className="col-span-1 grid gap-3">
              <div className="flex items-center justify-start mb-3">
                <div className="relative mr-2">
                  <span className="py-1 px-2 text-xs font-bold leading-none text-white rating-review-badge">
                    {rating}
                  </span>
                  <span className="absolute -top-15px -right-15px w-30px h-30px p- grid place-content-center rounded-full shadow border border-primary z-1 bg-white">
                    <span className="m-0.5 grid place-content-center">
                      <Image
                        src={logo_icon}
                        alt={title}
                        className="w-full h-full"
                      />
                    </span>
                  </span>
                </div>
              </div>
              <div className="flex items-center justify-start">
                <div className="relative mr-2">
                  <span className="py-1 px-2 text-xs font-bold leading-none text-white rating-review-badge">
                    {rating}
                  </span>
                  <span className="absolute -top-15px -right-15px w-30px h-30px p- grid place-content-center rounded-full shadow border border-primary z-1 bg-white">
                    <span className="m-1 grid place-content-center">
                      <Image
                        src={community_icon}
                        alt={title}
                        className="w-full h-full"
                      />
                    </span>
                  </span>
                </div>
              </div>
            </div>
          </div>
          <p className="text-gray-400 text-sm mt-3 ">{excerpt}</p>
        </div>
        <Link href={`/reviews/${slug}`} passHref>
          <a>
            <StyledButton1 cardTitle="view review" />
          </a>
        </Link>
      </div>
    </div>
  );
};

export default ReviewCard;
