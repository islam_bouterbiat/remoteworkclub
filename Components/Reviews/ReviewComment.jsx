import React from "react";
import RatingStars from "./RatingStars";
import { MdLocationPin } from "react-icons/md";

const ReviewComment = () => {
  return (
    <div className="border-b border-gray-300 last:border-none py-8">
      <div className="flex items-center">
        <RatingStars value={10} />
        <div className="flex items-center ml-6 text-gray-500 font-semibold">
          <span>Rustam,</span>
          <MdLocationPin className="ml-1" />
          <span>Ukranie</span>
        </div>
      </div>
      <span className="text-[#097DBE] text-xl font-semibold mt-2">
        Excellent
      </span>
      <div className="mt-5">
        <p className="text-gray-500">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut us
          justoas, pellentesque <br />
          eleifend libero at., blandit gravida condime <br />
          <br></br>
          <b className="text-black">What did you love about it?</b>
          <br />
          Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          <br />
          <br></br>
          <b className="text-black">What would you like to be better?</b>
          <br />
          eleifend libero at., blandit gravida condime
        </p>
      </div>
    </div>
  );
};

export default ReviewComment;
