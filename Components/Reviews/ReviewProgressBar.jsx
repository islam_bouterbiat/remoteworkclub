import React from "react";

const ReviewProgressBar = ({ name, progress }) => {
  return (
    <fieldset className="border border-solid border-gray-500 rounded pt-4 pb-6 px-6">
      <legend className="text-sm tracking-widest px-1">{name}</legend>
      <div className="w-full bg-gray-200 h-2 ">
        <div
          className="bg-gradient-to-r from-orange-500 via-yellow-400 to-green-400 h-2"
          style={{ width: `${progress}%` }}
        ></div>
      </div>
    </fieldset>
  );
};

export default ReviewProgressBar;
