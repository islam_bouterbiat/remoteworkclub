import React from "react";
import { CircularProgressbar } from "react-circular-progressbar";
import "react-circular-progressbar/dist/styles.css";

const ReviewProgressCircle = ({ progress }) => {
  return (
    <div className="px-10">
      <CircularProgressbar
        className="mb-3 circularprogressbar"
        strokeWidth="15"
        text={`${progress}%`}
        value={progress}
        styles={{
          path: {
            // Path color
            stroke: `#f87171`,
            // Whether to use rounded or flat corners on the ends - can use 'butt' or 'round'
            strokeLinecap: "butt",
          },
          text: {
            // Text color
            fill: "#000",
            // Text size
            fontSize: "16px",
          },
        }}
      ></CircularProgressbar>
    </div>
  );
};

export default ReviewProgressCircle;
