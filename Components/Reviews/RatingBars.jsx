import React from "react";

const RatingBars = () => {
  return (
    <div className=" py-2">
      <div className="mb-1 tracking-wide py-4 grid gap-3 max-w-sm">
        {/* <!-- first --> */}
        <div className="flex items-center mt-1">
          <div className=" w-1/5 text-[#5EC9A3] tracking-tighter">
            <span>5 star</span>
          </div>
          <div className="w-3/5">
            <div className="bg-gray-100 border w-full rounded-md h-6">
              <div className=" w-7/12 bg-[#5EC9A3] rounded-l-md h-6 mt-08 ml-08"></div>
            </div>
          </div>
          <div className="w-1/5 text-gray-700 pl-3">
            <span className="text-sm font-semibold">51%</span>
          </div>
        </div>
        {/* <!-- second --> */}
        <div className="flex items-center mt-1">
          <div className="w-1/5 text-[#5EC9A3] tracking-tighter">
            <span>4 star</span>
          </div>
          <div className="w-3/5">
            <div className="bg-gray-100 border w-full rounded-md h-6">
              <div className="w-1/5 bg-[#5EC9A3] rounded-l-md h-6 mt-08 ml-08"></div>
            </div>
          </div>
          <div className="w-1/5 text-gray-700 pl-3">
            <span className="text-sm font-semibold">17%</span>
          </div>
        </div>
        {/* <!-- third --> */}
        <div className="flex items-center mt-1">
          <div className="w-1/5 text-[#5EC9A3] tracking-tighter">
            <span>3 star</span>
          </div>
          <div className="w-3/5">
            <div className="bg-gray-100 border w-full rounded-md h-6">
              <div className=" w-3/12 bg-[#5EC9A3] rounded-l-md h-6 mt-08 ml-08"></div>
            </div>
          </div>
          <div className="w-1/5 text-gray-700 pl-3">
            <span className="text-sm font-semibold">19%</span>
          </div>
        </div>
        {/* <!-- 4th --> */}
        <div className="flex items-center mt-1">
          <div className=" w-1/5 text-[#5EC9A3] tracking-tighter">
            <span>2 star</span>
          </div>
          <div className="w-3/5">
            <div className="bg-gray-100 border w-full rounded-md h-6">
              <div className=" w-1/5 bg-[#5EC9A3] rounded-l-md h-6 mt-08 ml-08"></div>
            </div>
          </div>
          <div className="w-1/5 text-gray-700 pl-3">
            <span className="text-sm font-semibold">8%</span>
          </div>
        </div>
        {/* <!-- 5th --> */}
        <div className="flex items-center mt-1">
          <div className="w-1/5 text-[#5EC9A3] tracking-tighter">
            <span>1 star</span>
          </div>
          <div className="w-3/5">
            <div className="bg-gray-100 border w-full rounded-md h-6">
              <div className=" w-2/12 bg-[#5EC9A3] rounded-l-md h-6 mt-08 ml-08"></div>
            </div>
          </div>
          <div className="w-1/5 text-gray-700 pl-3">
            <span className="text-sm font-semibold">5%</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RatingBars;
