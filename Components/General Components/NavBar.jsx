import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import { useState, useEffect } from "react";
import { BiMenuAltRight } from "react-icons/bi";
import { AiOutlineClose } from "react-icons/ai";

import logo from "../../assets/images/logo.png";

const data = [
  { name: "Reviews", link: "reviews" },
  { name: "Guides", link: "guides" },
  { name: "Community", link: "community" },
  { name: "Inspirations", link: "inspiration" },
  { name: "Blog", link: "blogs" },
];

const NavBar = () => {
  const [menuOpen, setMenuOpen] = useState(false);
  const [size, setSize] = useState({
    width: undefined,
    height: undefined,
  });

  const router = useRouter();

  useEffect(() => {
    const handleResize = () => {
      setSize({
        width: window.innerWidth,
        height: window.innerHeight,
      });
    };
    window.addEventListener("resize", handleResize);

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  useEffect(() => {
    if (size.width > 768 && menuOpen) {
      setMenuOpen(false);
    }
  }, [size.width, menuOpen]);

  const menuToggleHandler = () => {
    setMenuOpen((p) => !p);
  };

  return (
    <div className="header mx-auto shadow-sm fixed w-full bg-white h-20 px-4">
      <div className="max-w-screen-2xl mx-auto flex justify-between items-center px:0 md:px-4 xl:px-2 py-4">
        {/* Logo */}
        <div className="md:float-left block">
          <Link href="/" passHref>
            <a>
              <Image
                src={logo}
                width={110}
                height={44}
                alt="Remote Work Club"
              />
            </a>
          </Link>
        </div>
        {/* Navs */}
        <div
          className={`flex-col md:flex-row flex md:block text-center items-center gap-4 fixed top-0 bottom-0 justify-center md:static w-full md:w-auto bg-[#000] opacity-95 md:bg-white md:opacity-100 translate-0 md:transform-none transition-transform ease-in-out duration-300 ${
            !menuOpen ? "translate-x-full" : ""
          }`}
        >
          {data.map((d, index) => (
            <Link key={index} href={`/${d.link}`} passHref>
              <span
                onClick={menuToggleHandler}
                className={
                  router.pathname == `/${d.link}`
                    ? "cursor-pointer md:text-gray-800 text-white hover:text-gray-800 inline-flex items-center px-4 py-2 gap-1 duration-100"
                    : "cursor-pointer text-gray-400 hover:text-gray-800 inline-flex items-center px-4 py-2 gap-1 duration-100"
                }
              >
                {d.name}
              </span>
            </Link>
          ))}
        </div>
        {/* Actions */}
        <div>
          <button className="hidden md:block whitespace-nowrap text-white font-bold py-2 px-4 rounded">
            Post a job
          </button>
        </div>
        {/* Mobile button goes here */}
        <div className="block md:hidden text-2xl transition duration-300 ease-in-out relative flex cursor-pointer">
          {!menuOpen ? (
            <div onClick={menuToggleHandler}>
              <svg
                width="44"
                height="44"
                viewBox="0 0 44 44"
                fill="none"
                xmlns="http://www.w3.org/2000/svg"
              >
                <circle cx="22" cy="22" r="22" fill="#14CDA1" />
                <path
                  d="M14.4998 17H29.4998C29.7208 17 29.9328 16.8683 30.0891 16.6339C30.2454 16.3995 30.3332 16.0815 30.3332 15.75C30.3332 15.4185 30.2454 15.1005 30.0891 14.8661C29.9328 14.6317 29.7208 14.5 29.4998 14.5H14.4998C14.2788 14.5 14.0669 14.6317 13.9106 14.8661C13.7543 15.1005 13.6665 15.4185 13.6665 15.75C13.6665 16.0815 13.7543 16.3995 13.9106 16.6339C14.0669 16.8683 14.2788 17 14.4998 17ZM29.4998 27H14.4998C14.2788 27 14.0669 27.1317 13.9106 27.3661C13.7543 27.6005 13.6665 27.9185 13.6665 28.25C13.6665 28.5815 13.7543 28.8995 13.9106 29.1339C14.0669 29.3683 14.2788 29.5 14.4998 29.5H29.4998C29.7208 29.5 29.9328 29.3683 30.0891 29.1339C30.2454 28.8995 30.3332 28.5815 30.3332 28.25C30.3332 27.9185 30.2454 27.6005 30.0891 27.3661C29.9328 27.1317 29.7208 27 29.4998 27ZM29.4998 20.75H14.4998C14.2788 20.75 14.0669 20.8817 13.9106 21.1161C13.7543 21.3505 13.6665 21.6685 13.6665 22C13.6665 22.3315 13.7543 22.6495 13.9106 22.8839C14.0669 23.1183 14.2788 23.25 14.4998 23.25H29.4998C29.7208 23.25 29.9328 23.1183 30.0891 22.8839C30.2454 22.6495 30.3332 22.3315 30.3332 22C30.3332 21.6685 30.2454 21.3505 30.0891 21.1161C29.9328 20.8817 29.7208 20.75 29.4998 20.75Z"
                  fill="white"
                />
              </svg>
            </div>
          ) : (
            <AiOutlineClose
              onClick={menuToggleHandler}
              className="text-white"
            />
          )}
        </div>
      </div>
    </div>
  );
};

export default NavBar;
