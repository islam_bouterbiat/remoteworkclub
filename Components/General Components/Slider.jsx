import React from "react";
import ExploreBar from "./ExploreBar";
import ReviewCard from "../Reviews/ReviewCard";
import GuideCard from "../Guides/GuideCard";
import InspirationCard from "../Inspirations/InspirationCard";
import BlogCard from "./../Blogs/BlogCard";

const components = {
  guides: GuideCard,
  reviews: ReviewCard,
  inspirations: InspirationCard,
  blogs: BlogCard,
};

const Slider = ({ sliderName, sliderExploreName, data, row }) => {
  const SpecificMenu = components[sliderName];

  const classes = row
    ? "md:pb-10 flex-nowrap flex overflow-x-auto md:grid grid-cols-1 sm:grid-cols-1 md:grid-cols-3 lg:grid-cols-3 xl:grid-cols-3 gap:1 md:gap-5 snap-mandatory snap-x "
    : "md:pb-10 flex md:flex-col snap-mandatory overflow-x-auto flex-nowrap snap-x";

  return (
    <div className="mb-10 md:px-12 mx-auto max-w-screen-lg 2xl:max-w-screen-xl">
      <ExploreBar name={sliderExploreName} />
      <div className={classes}>
        {data.map((d, i) => (
          <SpecificMenu key={i} data={d} row={row} />
        ))}
      </div>
      {/* Slider indicator goes here */}
    </div>
  );
};

export default Slider;
