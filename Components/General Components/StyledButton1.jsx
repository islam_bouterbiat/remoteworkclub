import React from "react";

const StyledButton1 = ({ cardTitle, classNameProp = "" }) => {
  return (
    <button className={"w-full text-white  h-10 mt-auto mb-0 " + classNameProp}>
      {cardTitle}
    </button>
  );
};

export default StyledButton1;
