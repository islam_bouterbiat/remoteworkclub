import React from "react";
import { RiArrowRightSLine } from "react-icons/ri";
import Link from "next/link";

const CategoriesNavs = () => {
  return (
    <Link href="#">
      <div className="relative w-72 h-14 2xl:w-80 2xl:h-16 flex items-center justify-between bg-gray-100 p-4  rounded shadow-md cursor-pointer">
        <span className="w-1 h-full bg-green-500 rounded-l absolute left-0"></span>
        <div className="font-semibold">Category name</div>
        <RiArrowRightSLine />
      </div>
    </Link>
  );
};

export default CategoriesNavs;
