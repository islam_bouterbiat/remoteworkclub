import React from "react";
import { BsArrowRightShort } from "react-icons/bs";

const ExploreBar = ({ name }) => {
  return (
    <div className="px-4 md:p-0">
      <div className="pb-1 flex justify-between items-center">
        <div>
          <h2 className="drop-shadow-lg">{name}</h2>
          <p className="hidden md:block drop-shadow-lg text-gray-400">
            Do you know what a cover letter should look like in 2021? See
          </p>
        </div>
        <button className="bg-white hover:bg-gray-500 text-gray-800 font-semibold hover:text-white py-2 md:px-4 md:border border-gray-200 hover:border-transparent rounded-full inline-flex items-center h-11 transition ease-in-out duration-200 hover:scale-105">
          <span>Explore all</span>
          <BsArrowRightShort className="md:mt-1" />
        </button>
      </div>
      <div className="explore-bar-border drop-shadow-lg"></div>
    </div>
  );
};

export default ExploreBar;
