import React from "react";
import Image from "next/image";
import Link from "next/link";
import moment from "moment";
import { MdOutlineDateRange } from "react-icons/md";
import StyledButton1 from "../General Components/StyledButton1";

import image from "../../assets/images/blog.jpg";

const BlogCard = ({
  data: { title, excerpt, Date, Tags, imageUrl, Author },
  row,
}) => {
  //dynamic flex class rendering for blogCard show according to the display of slider
  let flexClasses = "max-w-xs md:max-w-screen-xl overflow-hidden ";
  flexClasses += row ? "" : "md:flex";
  //dynamic padding class rendering for blogCard show according to the display of slider
  let paddingClass = "px-0 relative ";
  paddingClass += row ? "lg:px-2 " : "lg:px-6";
  // dynamic border class rendering for blogCard show according to the display of slider
  let borderClass = " h-full ";
  borderClass += row ? "" : "border-b border-1";

  return (
    <div className="py-10 px-2 flex-none snap-center  shadow-lg md:shadow-none">
      <div className={flexClasses}>
        <Link href="#">
          <a>
            <Image className="rounded-sm" src={image} alt={title} />
          </a>
        </Link>
        <div className={paddingClass}>
          <div className={borderClass}>
            <div className="px-2 flex justify-between">
              <span className=" text-sm text-gray-400">{Author}</span>
              <span className=" text-sm text-gray-400 flex items-center">
                <MdOutlineDateRange className="mr-1 mt-1" />
                {moment(Date).format("Do MMM YY")}
              </span>
            </div>
            <div className="px-2 py-4">
              {!row ? (
                <>
                  <div className="h-5/6 flex flex-col gap-4">
                    <div className="font-bold text-xl mb-2 ">{title}</div>
                    <p className="text-gray-400 text-sm grow mt-3 md:mb-6">
                      {excerpt}
                    </p>
                    <StyledButton1
                      cardTitle="View Blog"
                      classNameProp="lg:w-150px "
                    />
                  </div>
                  <div className="px-2 pt-4 pb-2 mb-2 md:absolute bottom-0">
                    <div className="hidden md:flex ">
                      {Tags.map((t, i) => (
                        <span
                          key={i}
                          className="inline-block rounded-full px-3 py-1 text-xs font-semibold mr-2 bg-gray-100 text-gray-400"
                        >
                          {t}
                        </span>
                      ))}
                    </div>
                  </div>
                </>
              ) : (
                <>
                  <div className="font-bold text-xl mb-2">{title}</div>
                  <p className="text-gray-400 text-sm mt-3 ">{excerpt}</p>
                  <div className="px-2 pt-4 pb-2 mb-2">
                    <div className="hidden md:flex ">
                      {Tags.map((t, i) => (
                        <span
                          key={i}
                          className="inline-block rounded-full px-3 py-1 text-xs font-semibold mr-2 bg-gray-100 text-gray-400"
                        >
                          {t}
                        </span>
                      ))}
                    </div>
                  </div>
                </>
              )}
            </div>
          </div>
        </div>
        {row ? (
          <a>
            <StyledButton1 cardTitle="view blog" />
          </a>
        ) : (
          ""
        )}
      </div>
    </div>
  );
};

export default BlogCard;
