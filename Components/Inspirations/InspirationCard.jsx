import React from "react";
import Image from "next/image";
import Link from "next/link";
import moment from "moment";
import { MdOutlineDateRange } from "react-icons/md";

import image from "../../assets/images/inspiration.jpg";

import StyledButton1 from "../General Components/StyledButton1";

const InspirationCard = ({
  data: { title, excerpt, Date, Author, Tags, imageUrl },
}) => {
  return (
    <div className="py-10 px-2 flex-none snap-center">
      <div className="max-w-xs overflow-hidden shadow-md">
        <Link href="#">
          <a>
            <Image src={image} alt={title} />
          </a>
        </Link>
        <div className="px-4 flex justify-between">
          <span className=" text-sm text-gray-400">{Author}</span>
          <span className=" text-sm text-gray-400 flex items-center">
            <MdOutlineDateRange className="mr-1 mt-1" />{" "}
            {moment(Date).format("Do MMM YY")}
          </span>
        </div>
        <div className="px-4 py-4">
          <div className="font-bold text-xl mb-2">{title}</div>
          <p className="text-gray-400 text-sm mt-3 ">{excerpt}</p>
        </div>
        <div className="px-4 pt-4 pb-2 mb-2">
          <div className="flex ">
            {Tags.map((t, i) => (
              <span
                key={i}
                className="inline-block rounded-full px-3 py-1 text-xs font-semibold mr-2 bg-gray-100 text-gray-400"
              >
                {t}
              </span>
            ))}
          </div>
        </div>
        <div>
          <StyledButton1 cardTitle="view inspiration" />
        </div>
      </div>
    </div>
  );
};

export default InspirationCard;
