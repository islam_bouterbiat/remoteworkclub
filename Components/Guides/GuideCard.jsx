import React from "react";
import Image from "next/image";
import Link from "next/link";

import image from "../../assets/images/stay-healthy.jpg";

import StyledButton1 from "../General Components/StyledButton1";

const GuideCard = ({ data: { title, Tags, imageUrl,slug } }) => {
  return (
    <div className='py-10 px-2 grid place-content-center flex-none snap-center'>
      <div className='max-w-xs overflow-hidden shadow-md grid h-450px'>
        <Link href='#'>
          <a>
            <Image src={image} alt={title} />
          </a>
        </Link>
        <span className='px-6 text-sm text-gray-400'>How to</span>
        <div className='px-6 pt-2'>
          <div className='font-bold text-xl mb-2'>{title}</div>
        </div>
        <div className='px-6 pt-4 pb-2 mb-2'>
          <div className='flex flex-wrap'>
            {Tags.map((t, i) => (
              <span
                key={i}
                className='whitespace-nowrap mb-1 inline-block rounded-full px-3 py-1 text-xs font-semibold mr-1 bg-gray-100 text-gray-400'>
                {t}
              </span>
            ))}
          </div>
        </div>
        <Link href={`/guides/${slug}`} passHref>
          <a>
            <StyledButton1 cardTitle="view guide" />
          </a>
        </Link>
      </div>
    </div>
  );
};

export default GuideCard;
